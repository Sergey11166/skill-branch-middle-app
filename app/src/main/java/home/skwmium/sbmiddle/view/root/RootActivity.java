package home.skwmium.sbmiddle.view.root;

import android.os.Bundle;
import android.support.annotation.Nullable;

import home.skwmium.sbmiddle.R;
import home.skwmium.sbmiddle.view.base.BaseActivity;

public class RootActivity extends BaseActivity {

    /*
    Отсутствует обрабока нажатия кнопки назад, в уроке вроде это делали и так понял это часть UX:
    Если поля ввода показаны, то при нажатии кнопки назад они должны анимировано свернуться.
    Возможно и я не так понял.
     */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
    }
}
