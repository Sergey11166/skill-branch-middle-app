package home.skwmium.sbmiddle.view.auth;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import home.skwmium.sbmiddle.BR;

@SuppressWarnings("WeakerAccess")
public class AuthViewModel extends BaseObservable {
    private boolean isAuthInProcess;
    private boolean isLoginState;
    private String email;
    private String password;
    private String emailErrorText;
    private String passwordErrorText;

    public void setAuthInProcess(boolean authInProcess) {
        isAuthInProcess = authInProcess;
        notifyPropertyChanged(BR.authInProcess);
    }

    public void setLoginState(boolean loginState) {
        isLoginState = loginState;
        notifyPropertyChanged(BR.loginState);
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(BR.password);
    }

    public void setEmailErrorText(String emailErrorText) {
        this.emailErrorText = emailErrorText;
        notifyPropertyChanged(BR.emailErrorText);
    }

    public void setPasswordErrorText(String passwordErrorText) {
        this.passwordErrorText = passwordErrorText;
        notifyPropertyChanged(BR.passwordErrorText);
    }

    @Bindable
    public boolean isAuthInProcess() {
        return isAuthInProcess;
    }

    @Bindable
    public boolean isLoginState() {
        return isLoginState;
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    @Bindable
    public String getEmailErrorText() {
        return emailErrorText;
    }

    @Bindable
    public String getPasswordErrorText() {
        return passwordErrorText;
    }
}
