package home.skwmium.sbmiddle.view.auth;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import home.skwmium.sbmiddle.R;
import home.skwmium.sbmiddle.databinding.FragmentAuthBinding;
import home.skwmium.sbmiddle.di.DaggerViewComponent;
import home.skwmium.sbmiddle.view.base.BaseFragment;
import home.skwmium.sbmiddle.view.custom.SimpleTextWatcher;

public class FragmentAuth extends BaseFragment implements IAuthView, View.OnClickListener {
    @Inject
    AuthPresenter presenter;
    private FragmentAuthBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerViewComponent.create().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentAuthBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.onViewCreated(this, savedInstanceState);
        init();
    }

    @Override
    public void showMessage(@StringRes int msgRes) {
        showSnackbar(msgRes);
    }

    @Override
    public void bindViewModel(AuthViewModel authViewModel) {
        binding.setModel(authViewModel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_social_fb:
                presenter.clickOnSocialFb();
                startAnimVibrate(binding.buttonSocialFb);
                break;
            case R.id.button_social_tw:
                presenter.clickOnSocialTw();
                startAnimVibrate(binding.buttonSocialTw);
                break;
            case R.id.button_social_vk:
                presenter.clickOnSocialVk();
                startAnimVibrate(binding.buttonSocialVk);
                break;
            case R.id.button_enter:
                presenter.clickOnLogin();
                break;
            case R.id.button_watch_menu:
                presenter.clickOnShowCatalog();
                break;
        }
    }

    private void init() {
        binding.buttonSocialFb.setOnClickListener(this);
        binding.buttonSocialTw.setOnClickListener(this);
        binding.buttonSocialVk.setOnClickListener(this);
        binding.buttonEnter.setOnClickListener(this);
        binding.buttonWatchMenu.setOnClickListener(this);

        binding.editEmail.addTextChangedListener(textWatcherEmail);
        binding.editPassword.addTextChangedListener(textWatcherPassword);
    }

    private final SimpleTextWatcher textWatcherEmail = new SimpleTextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            presenter.validateEmail();
        }
    };

    private final SimpleTextWatcher textWatcherPassword = new SimpleTextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            presenter.validatePassword();
        }
    };

    private void startAnimVibrate(@NonNull View view) {
        ObjectAnimator
                .ofFloat(view, "translationX", 0, 25, -25, 25, -25, 15, -15, 6, -6, 0)
                .setDuration(500)
                .start();
    }
}
