package home.skwmium.sbmiddle.view.auth;

import android.support.annotation.StringRes;

import home.skwmium.sbmiddle.view.IVIew;

interface IAuthView extends IVIew {
    void showMessage(@StringRes int msgRes);

    void bindViewModel(AuthViewModel authViewModel);
}
