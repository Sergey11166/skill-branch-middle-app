package home.skwmium.sbmiddle.view.auth;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import home.skwmium.sbmiddle.R;
import home.skwmium.sbmiddle.common.App;
import home.skwmium.sbmiddle.model.Model;
import home.skwmium.sbmiddle.utils.L;

import static android.util.Patterns.EMAIL_ADDRESS;
import static home.skwmium.sbmiddle.utils.Const.PASSWORD_MAX_LENGTH;
import static home.skwmium.sbmiddle.utils.Const.PASSWORD_MIN_LENGTH;

/*
Презентер, так же как и вью, лучше тоже разбивать на абстакцию и реалезацию.
 */
public class AuthPresenter {
    private static final AuthPresenter INSTANCE = new AuthPresenter();

    public static AuthPresenter getInst() {
        return INSTANCE;
    }

    @Inject
    Model model;

    //TODO избавиться от Resources в презентере?
    @Inject
    Resources resources;

    private WeakReference<IAuthView> weakAuthView;
    private AuthViewModel viewModel;

    public AuthPresenter() {
        App.getAppComponent().inject(this);
    }

    void onViewCreated(@NonNull IAuthView authView, @Nullable Bundle savedInstanceState) {
        weakAuthView = new WeakReference<>(authView);
        if (savedInstanceState == null || viewModel == null)
            viewModel = new AuthViewModel();
        authView.bindViewModel(viewModel);
    }

    @Nullable
    private IAuthView getView() {
        if (weakAuthView != null && !weakAuthView.isEnqueued())
            return weakAuthView.get();
        return null;
    }

    void validateEmail() {
        String email = viewModel.getEmail();
        if (!TextUtils.isEmpty(email) && !isEmailValid(email))
            viewModel.setEmailErrorText(resources.getString(R.string.error_invalid_email));
        else
            viewModel.setEmailErrorText(null);
    }

    void validatePassword() {
        String password = viewModel.getPassword();
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password))
            viewModel.setPasswordErrorText(resources.getString(R.string.error_invalid_password,
                    PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH));
        else
            viewModel.setPasswordErrorText(null);
    }

    void clickOnShowCatalog() {
        doSomeAction();
    }

    void clickOnLogin() {
        if (!viewModel.isLoginState()) {
            viewModel.setLoginState(true);
            return;
        }
        if (getView() != null && !isEmailValid(viewModel.getEmail())
                || !isPasswordValid(viewModel.getPassword())) {
            getView().showMessage(R.string.error_validation_data);
            return;
        }

        login();
    }

    void clickOnSocialFb() {
        doSomeAction();
        viewModel.setLoginState(false);
    }

    void clickOnSocialVk() {
        doSomeAction();
    }

    void clickOnSocialTw() {
        doSomeAction();
    }


    // ---------- UTIL ----------

    private void login() {
        viewModel.setAuthInProcess(true);
        //noinspection unchecked
        model.authorise(viewModel.getEmail(), viewModel.getPassword())
                .subscribe(o -> {
                        },
                        L::e,
                        () -> {
                            viewModel.setAuthInProcess(false);
                            if (getView() != null)
                                getView().showMessage(R.string.msg_success);
                        });
    }

    private void doSomeAction() {
        if (getView() == null) return;
        getView().showMessage(R.string.some_action);
    }

    private boolean isEmailValid(@Nullable String email) {
        return !TextUtils.isEmpty(email) && EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isPasswordValid(@Nullable String password) {
        return !TextUtils.isEmpty(password) && password.length() >= PASSWORD_MIN_LENGTH && password.length() <= PASSWORD_MAX_LENGTH;
    }
}
