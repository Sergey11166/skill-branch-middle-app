package home.skwmium.sbmiddle.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import home.skwmium.sbmiddle.utils.TypefaceHelper;

/*
Что то как то сложно выглядит этот способ установки кастомных шрифтов.
Чем плох вариант кастомной ТекстВью в которой через атрибуды устанавливаются шрифты?
 */
public class InflaterFactory implements LayoutInflater.Factory {
    private final LayoutInflater.Factory mBaseFactory;

    public InflaterFactory() {
        mBaseFactory = null;
    }

    public InflaterFactory(LayoutInflater.Factory baseFactory) {
        mBaseFactory = baseFactory;
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attributeSet) {
        if (name.equalsIgnoreCase(TextView.class.getSimpleName())) {
            return overrideTextViewIfNeed(context, attributeSet);
        }
        if (mBaseFactory != null)
            return mBaseFactory.onCreateView(name, context, attributeSet);
        return null;
    }

    private View overrideTextViewIfNeed(Context context, AttributeSet attributeSet) {
        TextView textView = new TextView(context, attributeSet);
        TypefaceHelper.applyFontToTextView(context, textView, attributeSet);
        return textView;
    }
}
