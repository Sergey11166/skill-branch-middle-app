package home.skwmium.sbmiddle.utils;

import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.support.design.widget.TextInputLayout;
import android.view.View;

public class BindingUtils {
    @BindingConversion
    public static int convertBooleanToVisibility(boolean visible) {
        return visible ? View.VISIBLE : View.GONE;
    }

    @BindingAdapter("errorText")
    public static void bindErrorMessage(TextInputLayout view, String errorMessage) {
        view.setErrorEnabled(errorMessage != null);
        view.setError(errorMessage);
    }
}
