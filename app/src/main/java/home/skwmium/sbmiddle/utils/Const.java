package home.skwmium.sbmiddle.utils;

public final class Const {

    // ---------- PREFS ----------
    public static final String IS_USER_AUTH = "is_user_auth";

    // ---------- OTHER ----------
    public static final int PASSWORD_MIN_LENGTH = 4;
    public static final int PASSWORD_MAX_LENGTH = 8;
}
