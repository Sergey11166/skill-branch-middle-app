package home.skwmium.sbmiddle.di;

import android.content.Context;
import android.content.res.Resources;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import home.skwmium.sbmiddle.model.Model;
import home.skwmium.sbmiddle.model.ModelImpl;
import rx.subscriptions.CompositeSubscription;

@Module
public class PresenterModule {

    @Provides
    @Singleton
    Model provideModel() {
        return new ModelImpl();
    }

    /*
    Если не пометить provide-метод scope анотацией, то эта зависимость будет создаваться новая
    каждый раз когда её используешь. Если подписки создаются в синглтоне, то CompositeSubscription и не нужен.
    Я вижу что эта зависимость пока не используется, это на будущее...
     */
    @Provides
    CompositeSubscription provideCompositeSubscription() {
        return new CompositeSubscription();
    }

    @Provides
    @Singleton
    Resources provideResources(Context context) {
        return context.getResources();
    }
}
