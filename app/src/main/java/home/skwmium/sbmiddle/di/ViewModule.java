package home.skwmium.sbmiddle.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import home.skwmium.sbmiddle.view.auth.AuthPresenter;

@Module
public class ViewModule {
    @Singleton
    @Provides
    public AuthPresenter provideAuthPresenter() {
        return AuthPresenter.getInst();
    }
}
