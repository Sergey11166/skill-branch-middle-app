package home.skwmium.sbmiddle.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import home.skwmium.sbmiddle.model.PreferencesManager;
import home.skwmium.sbmiddle.common.SchedulerProvider;

@Module
public class ModelModule {
    @Provides
    @Singleton
    PreferencesManager providePreferencesManager(Context context) {
        return new PreferencesManager(context);
    }

    @Provides
    @Singleton
    SchedulerProvider provideSchedulerProvider() {
        return SchedulerProvider.DEFAULT;
    }
}
