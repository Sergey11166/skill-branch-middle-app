package home.skwmium.sbmiddle.di;


import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    @NonNull
    private final Context context;

    public AppModule(@NonNull Context context) {
        this.context = context;
    }

    @NonNull
    @Singleton
    @Provides
    Context getContext() {
        return context;
    }
}
