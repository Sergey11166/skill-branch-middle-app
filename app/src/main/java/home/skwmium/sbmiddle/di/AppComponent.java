package home.skwmium.sbmiddle.di;

import javax.inject.Singleton;

import dagger.Component;
import home.skwmium.sbmiddle.model.ModelImpl;
import home.skwmium.sbmiddle.view.auth.AuthPresenter;

@Singleton
@Component(modules = {AppModule.class, PresenterModule.class, ModelModule.class})
public interface AppComponent {
    void inject(AuthPresenter authPresenter);

    void inject(ModelImpl model);
}
