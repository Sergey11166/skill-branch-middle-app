package home.skwmium.sbmiddle.di;

import javax.inject.Singleton;

import dagger.Component;
import home.skwmium.sbmiddle.view.auth.FragmentAuth;

@Singleton
@Component(modules = ViewModule.class)
public interface ViewComponent {
    void inject(FragmentAuth fragmentAuth);
}