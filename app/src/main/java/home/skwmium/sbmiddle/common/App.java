package home.skwmium.sbmiddle.common;

import android.annotation.SuppressLint;
import android.app.Application;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.widget.Toast;

import com.squareup.leakcanary.LeakCanary;

import home.skwmium.sbmiddle.di.AppComponent;
import home.skwmium.sbmiddle.di.AppModule;
import home.skwmium.sbmiddle.di.DaggerAppComponent;

@SuppressWarnings("unused")
public class App extends Application {
    private static App sInstance;
    private static AppComponent sAppComponent;

    private Toast mToast;

    @SuppressLint("ShowToast")
    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        mToast = Toast.makeText(this, null, Toast.LENGTH_SHORT);
        initServices();
    }

    public static App getInst() {
        return sInstance;
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public void showToast(@StringRes int res) {
        showToast(getString(res));
    }

    public void showToast(@Nullable String s) {
        if (s == null || s.isEmpty()) return;
        mToast.setText(s);
        mToast.show();
    }

    private void initServices() {
        sAppComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();

        LeakCanary.install(this);
    }
}
