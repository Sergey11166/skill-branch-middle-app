package home.skwmium.sbmiddle.model;

import android.support.annotation.NonNull;

import rx.Observable;

public interface Model {
    Observable<Boolean> isUserAuthorised();

    Observable authorise(@NonNull String login, @NonNull String password);
}
