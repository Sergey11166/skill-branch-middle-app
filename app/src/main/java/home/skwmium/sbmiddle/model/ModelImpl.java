package home.skwmium.sbmiddle.model;

import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import home.skwmium.sbmiddle.common.App;
import home.skwmium.sbmiddle.common.SchedulerProvider;
import home.skwmium.sbmiddle.utils.Const;
import rx.Observable;

public class ModelImpl implements Model {
    @Inject
    PreferencesManager preferencesManager;

    @Inject
    SchedulerProvider schedulerProvider;

    public ModelImpl() {
        App.getAppComponent().inject(this);
    }

    @Override
    public Observable<Boolean> isUserAuthorised() {
        return Observable.just(preferencesManager.get(Const.IS_USER_AUTH, false))
                .compose(schedulerProvider.applySchedulers());
    }

    @Override
    public Observable authorise(@NonNull String login, @NonNull String password) {
        return Observable.just(1)
                .doOnNext(integer -> makeUserAuthorised())
                .delay(3, TimeUnit.SECONDS)
                .compose(schedulerProvider.applySchedulers());
    }

    /*
    По заданию нужно реализовать сохранение токена, а не только факт успешной авторизации
     */
    private void makeUserAuthorised() {
        preferencesManager.put(Const.IS_USER_AUTH, true);
    }
}
